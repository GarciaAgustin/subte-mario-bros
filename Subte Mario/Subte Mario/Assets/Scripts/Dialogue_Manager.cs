using System.Collections;
using TMPro;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
public class Dialogue_Manager : MonoBehaviour
{
    public TextMeshProUGUI dialogueText;
    public string[] dialogues;
    private int currentIndex = 0;
    public float letterDelay = 0.05f; // Tiempo de retraso entre cada letra

    private Coroutine currentCoroutine;

    private bool canAdvanceDialogue = true; // Variable para controlar si se puede avanzar al siguiente di�logo

    void Start()
    {
        DisplayNextDialogue();
    }

    void Update()
    {
        if (canAdvanceDialogue && Input.GetKeyDown(KeyCode.Space)) // Verificar si se puede avanzar y se presion� la tecla Espacio
        {
            DisplayNextDialogue();
        }
    }

    public void DisplayNextDialogue()
    {
        if (currentIndex < dialogues.Length)
        {
            currentCoroutine = StartCoroutine(WriteDialogue(dialogues[currentIndex]));
            currentIndex++;
        }
        else
        {
            gameObject.SetActive(false);
            if(PlatformerGame_Manager.Instance == null)
            {
                SceneManager.LoadScene("Platformer_Scene");
            }
            else
            {
                PlatformerGame_Manager.Instance.GameStart = true;
            }
        }
    }

    IEnumerator WriteDialogue(string dialogue)
    {
        canAdvanceDialogue = false; // Desactivar la capacidad de avanzar al siguiente di�logo

        dialogueText.text = ""; // Limpiar el texto
        foreach (char letter in dialogue)
        {
            dialogueText.text += letter; // Agregar una letra al texto
            yield return new WaitForSeconds(letterDelay); // Esperar un tiempo antes de la siguiente letra
        }

        canAdvanceDialogue = true; // Activar la capacidad de avanzar al siguiente di�logo
        currentCoroutine = null; // Restablecer la corutina actual
    }
}
