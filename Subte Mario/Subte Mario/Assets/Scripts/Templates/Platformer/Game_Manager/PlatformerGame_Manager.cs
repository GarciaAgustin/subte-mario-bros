using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PlatformerGame_Manager : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI Timer;

    [SerializeField]
    float gametime;

    float InitalGameTime;

    public GameObject WinAndLosePanel;

    public bool ThePlayerHasWon;
    public bool ThePlayerLose;

    Color WinOrLoosePanelColor;

    public static PlatformerGame_Manager Instance;

    public bool GameStart = false;

    public GameObject WinTXT;
    public GameObject LoseTXT;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        InitalGameTime = gametime;

    }

    void Start()
    {

    }


    void Update()
    {

        if (ThePlayerHasWon == false && ThePlayerLose == false && GameStart == true)
        {
            gametime -= Time.deltaTime;

            Timer.text = gametime.ToString("F0");
        }

        if (gametime < 1)
        {
            ThePlayerLose = true;
        }

        ShowWinAndLoosePanel();

    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Platformer_Scene");
    }

    public void ShowWinAndLoosePanel()
    {
        if (ThePlayerHasWon == true)
        {
            Audio_Manager.instance.Play("Win");
            ColorUtility.TryParseHtmlString("#4DA454", out WinOrLoosePanelColor);

            WinAndLosePanel.GetComponent<Image>().color = WinOrLoosePanelColor;
            WinAndLosePanel.GetComponentInChildren<TextMeshProUGUI>().text = "You Win";
            Timer.gameObject.SetActive(false);
            WinAndLosePanel.gameObject.SetActive(true);
            WinTXT.gameObject.SetActive(true);
        }
        else if (ThePlayerLose == true)
        {
            Audio_Manager.instance.Play("Lose");
            ColorUtility.TryParseHtmlString("#9C2424", out WinOrLoosePanelColor);

            WinAndLosePanel.GetComponent<Image>().color = WinOrLoosePanelColor;
            WinAndLosePanel.GetComponentInChildren<TextMeshProUGUI>().text = "You Lose";
            Timer.gameObject.SetActive(false);
            WinAndLosePanel.gameObject.SetActive(true);
            LoseTXT.gameObject.SetActive(true);
        }

    }


}
