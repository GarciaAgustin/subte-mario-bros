using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using System.Collections;

public class Simple_PlayerController : MonoBehaviour
{
    private float horizontal;
    private float vertical;

    private Rigidbody2D rb;

    public bool isGrounded;
    public Transform groundCheck;
    public float playerSpeed;
    public float playerJumpForce;

    public LayerMask WhatIsGround;

    public float CheckRadius;

    public List<Gamepad> pads = new List<Gamepad>();

    private void Awake()
    {
        Audio_Manager.instance.Play("Music");
        rb = GetComponent<Rigidbody2D>();
       
    }

    void Start()
    {
      
        var devices = InputSystem.devices;

        foreach (var device in devices)
        {
            if (device is Gamepad gamepad)
            {
                Debug.Log("Gamepad detected: " + gamepad.displayName);
                pads.Add(gamepad);
            }
        }
    }

    private void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, CheckRadius, WhatIsGround);
    }

    private void Update()
    {
        if(PlatformerGame_Manager.Instance.GameStart == true)
        {
            Move();

            if (isGrounded == true)
            {
                gameObject.GetComponentInChildren<Animator>().SetBool("Player_Jump", false);
            }
            else
            {
                gameObject.GetComponentInChildren<Animator>().SetBool("Player_Jump", true);
            }
        }
    }

    private void Move()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        if (Keyboard.current.wKey.wasPressedThisFrame && isGrounded == true)
        {
            PerformJump();
        }

        rb.velocity = new Vector3(horizontal * playerSpeed, rb.velocity.y);

        if (horizontal < 0)
        {
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);
            gameObject.GetComponentInChildren<Animator>().SetBool("Player_Walk", true);
        }
        else if (horizontal > 0)
        {
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            gameObject.GetComponentInChildren<Animator>().SetBool("Player_Walk", true);
        }
        else
        {
            gameObject.GetComponentInChildren<Animator>().SetBool("Player_Walk", false);
        }
    }

    private void PerformJump()
    {
        Audio_Manager.instance.Play("Jump");
        gameObject.GetComponentInChildren<Animator>().SetBool("Player_Jump", true);
        rb.velocity = Vector2.up * playerJumpForce;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Cookie"))
        {
            Audio_Manager.instance.Play("Score");

            collision.gameObject.SetActive(false);
        }

        if (collision.CompareTag("TheChair"))
        {
            PlatformerGame_Manager.Instance.ThePlayerHasWon = true;
        }
    }
  
}
