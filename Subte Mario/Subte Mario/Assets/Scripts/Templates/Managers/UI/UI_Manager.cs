using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class UI_Manager : MonoBehaviour
{

    public static UI_Manager instance;

    [SerializeField]
    GameObject menu;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }


        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        
    }

    public void SliderChanged()
    {
    }



    private void Update()
    {
        
    }



    public void GotoItchio()
    {
        Application.OpenURL("https://live-games.itch.io/");
    }

    public void Exit()
    {
        Application.Quit();
    }


    public void GoToCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
    }


    public void GotoPlataformerScene()
    {
        SceneManager.LoadScene("Sleep");
    }

    public void GotoFirstPersonScene()
    {
        SceneManager.LoadScene("FirstPerson_Scene");
    }
}
