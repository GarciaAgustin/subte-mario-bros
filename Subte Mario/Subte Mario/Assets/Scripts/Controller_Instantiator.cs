using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
   public List<GameObject> enemies;
    public GameObject instantiatePos; // Crea las variables que se van a usar
    public float respawningTimer;
    private float time = 0;

    void Start()
    {
        Enemy_Movement.speed = 3;
    }

    void Update()
    {
        if(PlatformerGame_Manager.Instance.GameStart == true)
        {
            SpawnEnemies();
        }
    }

    private void ChangeVelocity()
    {
        time += Time.deltaTime;
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime; 

        if (respawningTimer <= 0) 
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform); 

            respawningTimer = UnityEngine.Random.Range(2, 6); 
        }


    }

}
