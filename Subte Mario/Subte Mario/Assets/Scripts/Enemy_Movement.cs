using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Movement : MonoBehaviour
{
    [SerializeField]
    public static float speed = 5.0f;
    private Rigidbody2D rb;
    private SpriteRenderer sprite;

    public enum TypeOfDirection {left,right};
    public TypeOfDirection type;
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        ChangeColor();
    }

    private void Update()
    {
        if(type == TypeOfDirection.left)
        {
            rb.AddForce(new Vector3(-speed, 0, 0), ForceMode2D.Force);
        }
        if (type == TypeOfDirection.right)
        {
            rb.AddForce(new Vector3(speed, 0, 0), ForceMode2D.Force);
        }
        OutOfBounds();

    }

    public void OutOfBounds()
    {
        if (this.transform.position.x <= -101.8)
        {                                      
            Destroy(this.gameObject);
        }
        else if (this.transform.position.x >= 356.9)
        {
            Destroy(this.gameObject);
        }
    }


    void ChangeColor()
    {
        Color newColor = new Color(Random.value, Random.value, Random.value, 1.0f);
        sprite.color = newColor;
    }
}
